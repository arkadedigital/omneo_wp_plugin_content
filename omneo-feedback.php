<?php

namespace Omeno\Content\Feedback;

use Omeno\Core;
use Omeno\Content;



function get_content_feedback($post, $type)
{
    global $CONTENT_TYPE_LIST;

    $acf = get_fields($_GET['post']);


    $omneo_id = $acf['omneo_id'];

    $content_type = $CONTENT_TYPE_LIST[$acf['content_type']];

    $content_item_id = \Omneo\Content\get_content_item_id($omneo_id, $content_type);




    if(empty($content_item_id) || empty($omneo_id))
    {
       return '0';
    }else
    {

        if($type == 'contentitemratings')
        {
            return get_content_ratings($content_item_id);
        }else
        {

            $args['verb'] = 'get';
            $args['api_request'] = $type;
            $args['data'] = array(
                "content_item_id" => $content_item_id,
                "count" => 1
            );

            $response = \Omneo\Core\send_request($args);

            //Core\write_to_log($response);

            return $response['data'];
        }

    }

}

function get_content_ratings($content_item_id)
{

    if(!$content_item_id)
    {
        die('Missing param : content_item_id');
    }
    $rating_types = \Omneo\Content\get_content_rating_types();

//    var_dump($rating_types);

        $rating_list = array();
    foreach($rating_types as $rt)
    {
        $args['verb'] = 'get';
        $args['api_request'] = "contentitemratings";
        $args['data'] = array(
            "content_item_id" => $content_item_id,
            "rating_type_id" => $rt['id']
        );

        $response = \Omneo\Core\send_request($args);



        if(!empty($response['data']))
        {
            $rating_list[$rt['id']] = $rt;

            $rating_list[$rt['id']]['responses'] = $response['data'];

        }

        //\Omneo\Core\write_to_log($rating_list);
    }
        return $rating_list;
}





function menu()
{
    add_submenu_page(
        null,
        'Comments',
        'Comments',
        'manage_options',
        'list-comments',
        __NAMESPACE__ . '\\list_comments'
    );

    add_submenu_page(
        null,
        'Ratings',
        'Ratings',
        'manage_options',
        'list-ratings',
        __NAMESPACE__ . '\\list_ratings'
    );

}
add_action('admin_menu', __NAMESPACE__ . '\\menu');

function list_comments()
{
    // Load page
    require_once(\Omneo\Content\__PATH() . 'feedback/list-comments.php');
}
function list_ratings()
{
    // Load page
    require_once(\Omneo\Content\__PATH() . 'feedback/list-ratings.php');
}
