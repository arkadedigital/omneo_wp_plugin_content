jQuery(function($) {
    // Disable Omneo Resource ID field
    $('#acf-field_5611e1e45b5c7').prop('disabled', true);

    // Disable content type if omneo id set
    if($('#acf-field_5611e1e45b5c7').val())
    {
        $('#acf-field_53227d84da8b0').prop('disabled', true);
    }

    // Disable Omneo Resource ID field
    $('#acf-field_56787395455ca').prop('disabled', true);

    // Disable content type if omneo id set
    if($('#acf-field_56787395455ca').val())
    {
        $('#acf-field_56787395455ca').prop('disabled', true);
    }

    // Disable Omneo Resource ID field
    $('#acf-field_565d18a49d7b7').prop('disabled', true);

    // Disable content type if omneo id set
    if($('#acf-field_565d18a49d7b7').val())
    {
        $('#acf-field_565d18a49d7b7').prop('disabled', true);
    }

    // Add placeholder text to publishing schedule fields
    $('.acf-field-56aaa4cf2565c input[type=text]').attr('placeholder', 'Now');
    $('.acf-field-56aaa5102565d input[type=text]').attr('placeholder', 'Never');
});
