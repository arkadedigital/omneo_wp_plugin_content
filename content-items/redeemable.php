<?php namespace Omneo\Content\Item\Redeemable;

use Omneo\Core;
use Omneo\Content;

defined('ABSPATH') or die('Access Denied');


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 * @param $post
 * @param $updated
 */
function save_post($post_id, $post, $updated)
{
    $content_type_id = get_field('content_type', $post_id);
    $acf = get_fields($post_id);
    $ri_image_id = array_shift(get_post_meta($post_id, 'ri_image_id')); // image omneo resource id
    $ri_thumbnail_cant_afford_image_id = array_shift(get_post_meta($post_id, 'ri_thumbnail_cant_afford_image_id')); // image omneo resource id
    $ri_thumbnail_can_afford_image_id = array_shift(get_post_meta($post_id, 'ri_thumbnail_can_afford_image_id')); // image omneo resource id
    $ri_thumbnail_redeemed_image_id = array_shift(get_post_meta($post_id, 'ri_thumbnail_redeemed_image_id')); // image omneo resource id

    if ($post->post_type == 'content_items' && $updated && $content_type_id == 11) {
        $data = [
            'sku' => $acf['ri_sku'],
            'max_quantity_redeemable' => $acf['ri_max_quantity_redeemable'],
            'points' => $acf['ri_price'],
            'image_url' => isset($acf['ri_image']['url']) ? $acf['ri_image']['url'] : '',
            'thumbnail_cant_afford_image_url' => isset($acf['ri_thumbnail_cant_afford_image']['url']) ? $acf['ri_thumbnail_cant_afford_image']['url'] : '',
            'thumbnail_can_afford_image_url' => isset($acf['ri_thumbnail_can_afford_image']['url']) ? $acf['ri_thumbnail_can_afford_image']['url'] : '',
            'thumbnail_redeemed_image_url' => isset($acf['ri_thumbnail_redeemed_image']['url']) ? $acf['ri_thumbnail_redeemed_image']['url'] : '',
        ];

        // Save thumbnails (omneo image endpoint)
        $images = array('image', 'thumbnail_cant_afford_image', 'thumbnail_can_afford_image', 'thumbnail_redeemed_image');
        foreach ($images as $image) {

            if (!$acf['ri_' . $image] && ${'ri_' . $image . '_id'}) { // Delete

                $image_response = Core\send_request([
                    'api_request' => 'content/images/' . ${'ri_' . $image . '_id'},
                    'verb' => 'delete'
                ]);
                if (isset($image_response['data'])) {
                    delete_post_meta($post_id, 'ri_' . $image . '_id', $image_response['data'][0]['id']);
                    $data[$image . '_id'] = '';
                }
            } elseif ($acf['ri_' . $image] && empty(${'ri_' . $image . '_id'})) { // Create
                $image_response = Core\send_request([
                    'api_request' => 'content/images',
                    'verb' => 'post',
                    'data' => [
                        'title' => 'Redeemable Image - ' . $image,
                        'src_height' => $acf['ri_' . $image]['height'],
                        'src_width' => $acf['ri_' . $image]['width'],
                        'item_reference' => $acf['ri_' . $image]['url']
                    ]
                ]);

                if (isset($image_response['data'])) {
                    if (${'ri_' . $image . '_id'}) {
                        update_post_meta($post_id, 'ri_' . $image . '_id', $image_response['data'][0]['id']);
                    } else {
                        add_post_meta($post_id, 'ri_' . $image . '_id', $image_response['data'][0]['id'], 1);
                    }

                    if ($image_response['data'][0]) {
                        $data[$image . '_id'] = $image_response['data'][0]['id'];
                    } else {
                        $data[$image . '_id'] = array_shift($image_response['data']);
                    }
                }
            } elseif ($acf['ri_' . $image] && ${'ri_' . $image . '_id'}) { // Update

                $update_data = [
                    'api_request' => 'content/images/' . ${'ri_' . $image . '_id'},
                    'verb' => 'put',
                    'data' => [
                        'src_height' => $acf['ri_' . $image]['height'],
                        'src_width' => $acf['ri_' . $image]['width'],
                        'item_reference' => $acf['ri_' . $image]['url']
                    ]
                ];

                $image_response = Core\send_request($update_data);

                if ($image_response['error']) {
                    if ($image_response['error']['http_code'] != 404) {
                        if ($image_response['data'][0]) {
                            $data[$image . '_id'] = $image_response['data'][0]['id'];
                        } elseif (array_shift($image_response['data'])) {
                            $data[$image . '_id'] = array_shift($image_response['data']);
                        }
                    }
                }

                //update the thumbnail {whatever} image id
                if ($image_response['data'] && !$image_response['data'][0]) {
                    $data[$image . '_id'] = ($image_response['data']['id']);
                }
            }
        }

        $r = Content\process_request([
            'post_id' => $post_id,
            'post' => $post,
            'data' => $data
        ]);
    }
}

add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);
add_action('before_delete_post', '\\Omneo\Content\delete_content_item', 10, 3);