<?php namespace Omneo\Content\Item\Lookbook;

use Omneo\Core;
use Omneo\Content;

defined('ABSPATH') or die('Access Denied');


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 */
function save_post($post_id, $post, $updated)
{
    $content_type_id = get_field('content_type', $post_id);

    if ($post->post_type == 'content_items' && $updated && $content_type_id == 7 ) {

        $acf = get_fields($post_id);
        $i = 1;



        /*
         *  https://arkade.atlassian.net/wiki/display/OmneoCore/JSON+Examples
            "images":
            [{
                "title":"",
                "description":"lookbook image1",
                "item_reference":"hptt://valid-domain-lookbook1.com/image1.jpg",
                "links": [{
                    "title":"link1",
                    "description":"image1link description",
                    "link_type":"external",
                    "link":"http://valid-link.com/link",
                    "price":"100",
                    "availibility":"1",
                    "external_link_load":1
                },
                {
                    "title":"link2",
                    "description":"image1link description",
                    "link_type":"external",
                    "link":"http://valid-link.com/link",
                    "price":"100",
                    "availibility":"0",
                    "external_link_load":1
                }]
            }]

         */

        foreach($acf['lookbook'] as $img)
        {
            $links = array();
            $img_data['title'] = $img['image_name'];
            $img_data['description'] = $img['image_description'];
            $img_data['item_reference'] = $img['image']['url'];
            $img_data['height'] = $img['image']['height'];
            $img_data['width'] = $img['image']['width'];

            if(!empty($img['links']))
            {

                foreach($img['links'] as $link)
                {
                    $link_data['title'] = $link['name'];
                    $link_data['description'] = 'Link Description For Link - ' . $link['name'];
                    $link_data['link_type'] = $link['link_type'] == 1 ? 'internal' : 'external';
                    $link_data['link'] = $link['external_link'] ;
                    $link_data['internal_content_item_id'] = get_field('omneo_id', $link['link_to']);
                    $link_data['price'] = $link['price'];
                    $link_data['external_link_load'] = $link['external_link_load'];
                    $link_data['availability'] = (int)$link['buy_store'];

                    // Google Analytics
                    $link_data['use_google_analytics'] = $img['content_ga'];
                    $link_data['utm_content'] = $img['content_ga_utm_content'];
                    $link_data['utm_campaign'] = $img['content_ga_utm_campaign'] ?: get_field('ga_utm_campaign', 'option');
                    $link_data['utm_source'] = get_field('ga_utm_source', 'option'); // Global value
                    $link_data['utm_medium'] = get_field('ga_utm_medium', 'option'); // Global value
                    $link_data['utm_term'] = $img['content_ga_utm_term'];

                    $links[] = $link_data;
                }
            }



            $img_data['links'] = $links;


            $images[] = $img_data;
            $i++;
        }

        //var_dump($links);





        $omneo_id = get_field('omneo_id', $post_id);

        if($omneo_id != '' && $omneo_id > 0)
        {
            $args = [
                'api_request' => 'content/lookbooks/'.$omneo_id ,
                'verb' => 'delete'
            ];
            $response = Core\send_request($args);
            if($response['data']['deleted'])
            {
                update_field('field_5611e1e45b5c7', '');

            }else
            {
                $msg = array(
                    'post_id' => $post_id,
                    'omneo_id' => $omneo_id,
                    'message' => 'Could Not Delete Omneo Lookbook Record'
                );
                loggly($msg, 'omneo-wp-content-error');
            }
        }

        // Data
        $data = [
            // Optional
            'lookbook_items' => json_encode(array('images' =>  $images))
        ];

/*       echo '<pre>';
        print_r($acf);
        print_r($data);
        print_r($images);
        die() ;*/

        Content\process_request([
            'post_id' => $post_id,
            'post' => $post,
            'data' => $data
        ]);

    }
}

add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);
add_action('before_delete_post', '\\Omneo\Content\delete_content_item', 10, 3);