<?php namespace Omneo\Content\Item\Links;

use Omneo\Core;
use Omneo\Content;

defined('ABSPATH') or die('Access Denied');


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 * @param $post
 * @param $updated
 */
function save_post($post_id, $post, $updated)
{

    $content_type_id = get_field('content_type', $post_id);

    if ($post->post_type == 'content_items' && $updated && $content_type_id == 3) {

        $acf = get_fields($post_id);

        $data = [];

        $data['link_type'] = ($acf['single_link_type'] == 0) ? 'internal' : 'external';
        if ($data['link_type'] == 'internal') {
            $post_object = get_field('single_link_to', $post_id);

            $data['internal_content_item_id'] = get_field('omneo_content_item_id', $post_object->ID);
        }

        $data['link'] = $acf['single_external_link'];
        $data['external_link_load'] = $acf['single_external_link_load'];

        Content\process_request([
            'post_id' => $post_id,
            'post' => $post,
            'data' => $data
        ]);
    }
}

add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);
add_action('before_delete_post', '\\Omneo\\Content\\delete_content_item', 10, 3);


function webhook($data)
{
    /*
     {
        "id": 4,
        "content_item_id": 102,
        "link_type": "external",
        "link": "",
        "external_link_load": 0,
        "internal_content_item_id": 52,
        "created_at": 1448846707,
        "updated_at": 1448846742,
        "deleted_at": null,
        "content_item_attributes": {
            "id": 102,
            "content_type": "link",
            "title": "Recognising fantastic support",
            "description": "",
            "intro_content": "Jed Nevad, Senior Technical Architect from the IT Infrastructure team in Hawthorn was recognised for the fantastic support he constantly provides to the Corporate Human Resources team. He was especially helpful when the Training & Learning Sytem (TLS) crashed a few weeks ago \u2013 he got it up and running again in no time, without impact to the system or wider business.\r\nPhoto above: Kate Black, HR System Advisor presenting Jed with his LOV Award",
            "published": 1,
            "thumbnail_on_image_url": "",
            "thumbnail_on_image_id": null,
            "thumbnail_off_image_url": "http:\/\/orora-prod-wp-omneo-uploads.s3-ap-southeast-2.amazonaws.com\/wp-content\/uploads\/2015\/11\/29234303\/image0031.jpg",
            "thumbnail_off_image_id": 39,
            "thumbnail_lock_image_url": "",
            "thumbnail_lock_image_id": null,
            "guest_only": 1,
            "is_public": 1,
            "allow_comments": 1,
            "allow_likes": 1,
            "allow_ratings": 1,
            "use_google_analytics": 0,
            "utm_content": null,
            "utm_source": null,
            "utm_medium": null,
            "utm_campaign": null,
            "item_order": 0,
            "visibility": "open",
            "created_at": 1448846707,
            "updated_at": 1448846750,
            "deleted_at": null,
            "thumbnail_on_image_url_attributes": null,
            "thumbnail_off_image_url_attributes": {
                "id": 39,
                "content_item_id": 103,
                "item_reference": "http:\/\/orora-prod-wp-omneo-uploads.s3-ap-southeast-2.amazonaws.com\/wp-content\/uploads\/2015\/11\/29234303\/image0031.jpg",
                "src_height": 168,
                "src_width": 224,
                "created_at": 1448846750,
                "updated_at": 1448846750,
                "deleted_at": null,
                "managed_asset_reference": null,
                "managed_asset": 0,
                "item_reference_sml": null,
                "item_reference_med": null,
                "item_reference_lrg": null
            },
            "thumbnail_lock_image_url_attributes": null
        },
        "content_item_locations": []
    }
     */

//    var_dump($data);
    $content_type_id = 3;

    $meta_query = array(
        'relation' => 'AND', // Optional, defaults to "AND"
        array(
            'key' => 'omneo_id',
            'value' => $data['id'],
            'compare' => '='
        )
    );

    $args = array(
        'post_type' => 'content_items',
        'meta_query' => $meta_query,
        'post_status' => 'any'
    );

    $posts = new \WP_Query($args);

    // Set defaults to API
    Core\send_request([
        'verb' => 'put',
        'api_request' => 'content/links/' . $data['id'],
        'data' => [
            '_source' => 'wp', // Required to flag that this is updated from wp so that a duplicate webhook doesn't get created
            'use_google_analytics' => 1,
            'promoted' => 1,
            'utm_source' => get_field('ga_utm_source', 'option'),
            'utm_medium' => get_field('ga_utm_medium', 'option'),
            'utm_campaign' => get_field('ga_utm_campaign', 'option')
        ]
    ]);

    try {

        $post_data = array(
            'post_title' => $data['content_item_attributes']['title'],
            'post_content' => '',
            'post_type' => 'content_items',
            'post_status' => 'publish',

        );

        if ($posts->post_count > 0) {
            $post_data['ID'] = $posts->posts[0]->ID;
        }

        if (get_field('omneo_updated_at', $posts->posts[0]->ID) != $data['content_item_attributes']['updated_at']) {
            $post_id = wp_insert_post($post_data, true);

            $link_type = array(
                'external' => 1,
                'internal' => 0
            );

            update_field('field_53228d0db99f9', $link_type[strtolower($data['link_type'])], $post_id);  //single_link_type
            update_field('field_53228e33b99fe', $data['link'], $post_id);  //single_external_link
            update_field('field_532b62ac32c82', $data['external_link_load'], $post_id);  //external_link_load
            update_field('field_56ce6e6511a65', 1, $post_id);  //external_link_load


            // update_field('field_5611e1e45b5c7', $data['id'], $post_id);  //external_link_load

            Content\set_common_post_acf_fields($data, $content_type_id, $post_id);

            wp_publish_post($post_id);
            write_to_log('FOUND POST BUT UPDATED-AT STAMPS HAVE CHANGED. UPDATED POST');

        } else {
            write_to_log('FOUND POST BUT UPDATED-AT STAMPS ARE THE SAME');
        }


        http_response_code(200);
//        exit;
    } catch (Exception $e) {
        Core\loggly('omneo-webbook', $e->getMessage());
        print_r($e->getMessage());
    }


}
