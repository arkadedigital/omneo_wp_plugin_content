<?php namespace Omneo\Content\Item\Html;

use Omneo\Core;
use Omneo\Content;

defined('ABSPATH') or die('Access Denied');


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 * @param $post
 * @param $updated
 */
function save_post($post_id, $post, $updated)
{
    $content_type_id = get_field('content_type', $post_id);

    if ($post->post_type == 'content_items' && $updated && $content_type_id == 1 ) {
        // Data
        $data = [
            // Optional
            'html' => get_field('html', $post_id),
        ];

        Content\process_request([
            'post_id' => $post_id,
            'post' => $post,
            'data' => $data
        ]);
    }
}


add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);
add_action('before_delete_post', '\\Omneo\Content\delete_content_item', 10, 3);



function webhook($data)
{

    /*
     {
        "data": {
        "id": 4,
        "content_item_id": 4,
        "html": "<p>Hey there!</p>",
        "created_at": "1443413387",
        "updated_at": "1443413387",
        "deleted_at": null,
        "content_item_attributes": {
            "id": 4,
            "content_type": "html",
            "title": "HTML Block 1",
            "description": "HTML Block for Arkade Loyalty Agency",
            "intro_content": null,
            "thumbnail_on_image_url": null,
            "thumbnail_off_image_url": null,
            "thumbnail_lock_image_url": null,
            "allow_comments": 1,
            "allow_likes": 1,
            "allow_ratings": 0,
            "created_at": "1443413387",
            "updated_at": "1443413387",
            "deleted_at": null
            }
        },
        "elapsed_sec": 0.051949024200439
        }

     */

    $content_type_id = 1;
    try
    {
        $post_data = array(
           'post_title' => $data['content_item_attributes']['title'],
            'post_content' => '',
            'post_type' => 'content_items'

        );

        $post_id = wp_insert_post( $post_data, true);

        update_field('field_5613149661a63', $data['html'], $post_id);

        Content\set_common_post_acf_fields($data, $content_type_id, $post_id);

        wp_publish_post($post_id);
    }
    catch(Exception $e)
    {
        Core\loggly('omneo-webbook', $e->getMessage());
        print_r($e->getMessage());
    }




}