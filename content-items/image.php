<?php namespace Omneo\Content\Item\Image;

use Omneo\Core;
use Omneo\Content;

defined('ABSPATH') or die('Access Denied');


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 * @param $post
 * @param $updated
 */
function save_post($post_id, $post, $updated)
{
    $content_type_id = get_field('content_type', $post_id);

    $acf = get_fields($post_id);
    if ($post->post_type == 'content_items' && $updated && $content_type_id == 9 )
    {


        $image = $acf['single_image'];

        $data = [
            // Optional
            'item_reference' => isset($image['url']) ? $image['url'] : '',
            'src_height' => $image['height'],
            'src_width' => $image['width'],
        ];


       $r =  Content\process_request([
            'post_id' => $post_id,
            'post' => $post,
            'data' => $data
        ]);

    }
}

add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);
add_action('before_delete_post', '\\Omneo\Content\delete_content_item', 10, 3);