<?php namespace Omneo\Content\Item\Event;

use Omneo\Core;
use Omneo\Content;

defined('ABSPATH') or die('Access Denied');


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 * @param $post
 * @param $updated
 */
function save_post($post_id, $post, $updated)
{
    $acf = get_fields($post_id);

    if ($post->post_type == 'content_items' && $updated && $acf['content_type'] == 6 ) {
        // Data
        $data = [
            // Mandatory
            'event_url' => $acf['event_url'],
            'event_start' => strtotime($acf['start_date']),
            'event_end' => strtotime($acf['end_date']),

            // Optional
            'location' => $acf['location'],
            'event_notes' => $acf['notes']
        ];

        Content\process_request([
            'post_id' => $post_id,
            'post' => $post,
            'data' => $data
        ]);
    }
}
add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);
add_action('before_delete_post', '\\Omneo\Content\delete_content_item', 10, 3);