<?php namespace Omneo\Content\Item\Gallery;

use Omneo\Core;
use Omneo\Content;

defined('ABSPATH') or die('Access Denied');


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 * @param $post
 * @param $updated
 */
function save_post($post_id, $post, $updated)
{
    $content_type_id = get_field('content_type', $post_id);

    if ($post->post_type == 'content_items' && $updated && $content_type_id == 2) {

        $acf = get_fields($post_id);

        $i = 1;
        foreach ($acf['gallery'] as $img) {
            $img_data['title'] = $post->post_title . ' - image ' . $i;
            $img_data['description'] = $post->post_title . ' - image ' . $i;
            $img_data['item_reference'] = $img['image']['url'];
            $img_data['height'] = $img['image']['height'];
            $img_data['width'] = $img['image']['width'];
            $img_data['src_height'] = $img['image']['height'];
            $img_data['src_width'] = $img['image']['width'];
            $img_data['enable_link'] = $img['enable_link'];

            $img_data['link']['title'] = $post->post_title . ' - link  ' . $i;
            $img_data['link']['description'] = $post->post_title . ' - link  ' . $i;
            $img_data['link']['link_type'] = $img['link_type'] == 0 ? 'internal' : 'external';
            $img_data['link']['internal_content_item_id'] = get_field('omneo_id', $acf['link_to']->ID);
            $img_data['link']['external_link_load'] = $img['external_link_load'];
            $img_data['link']['external_link'] = $img['external_link'];
            $img_data['link']['link'] = $img['external_link'];

            // Google Analytics
            $img_data['link']['use_google_analytics'] = $img['content_ga'];
            $img_data['link']['utm_content'] = $img['content_ga_utm_content'];
            $img_data['link']['utm_campaign'] = $img['content_ga_utm_campaign'] ?: get_field('ga_utm_campaign', 'option');
            $img_data['link']['utm_source'] = get_field('ga_utm_source', 'option'); // Global value
            $img_data['link']['utm_medium'] = get_field('ga_utm_medium', 'option'); // Global value
            $img_data['link']['utm_term'] = $img['content_ga_utm_term'];

            $images[] = $img_data;
            $i++;
        }

        $omneo_id = get_field('omneo_id', $post_id);

        /*PUT does not work on gallery items

        to update a gallery item
        1. POST a new image
        2. get the image ID
        3. POST to gallery items with image id & gallery ID

        repeat above if there are links on the image

        */


        /*
                $args = [
                    'api_request' => 'content/gallery/'.$omneo_id ,
                    'verb' => 'get'
                ];

                $gallery_items = Core\send_request($args);
                //var_dump($gallery_items);

                foreach($gallery_items['data']['gallery_items'] as $g_item)
                {
                    //delete the items;
                    $args = [
                        'api_request' => 'content/galleryitems/'.$g_item['id'] ,
                        'verb' => 'delete'
                    ];
                    //var_dump($args);
                    Core\send_request($args);
                }*/


        if ($omneo_id != '' && $omneo_id > 0) {
            $args = [
                'api_request' => 'content/gallery/' . $omneo_id,
                'verb' => 'delete'
            ];
            $response = Core\send_request($args);
            if ($response['data']['deleted']) {
                update_field('field_5611e1e45b5c7', '');

            } else {
                $msg = array(
                    'post_id' => $post_id,
                    'omneo_id' => $omneo_id,
                    'message' => 'Could Not Delete Omneo Gallery Record'
                );
                Core\loggly($msg, 'omneo-wp-content-error');
                exit;
            }
        }

        // Data
        $data = [
            // Optional
            'gallery_items' => json_encode(array('images' => $images))
        ];


        $response = Content\process_request([
            'post_id' => $post_id,
            'post' => $post,
            'data' => $data
        ]);


        /*  if($response['data']['gallery_item_link']['id'])
          {


              $args['api_request'] = 'content/links/' .$response['data']['link_details']['id'];
              $args['data'] = $link;
              $args['verb'] = 'put';


              $link_response = Core\send_request($args);

          }*/


        //Core\write_to_log(print_r($data,true));
        // Core\write_to_log(print_r($api_resp,true));
        //die();

        /*        $gal_images = array();
                foreach($api_resp['data']['gallery_items'] as $gal_image)
                {
                    $gal_images[] = $gal_image['id'];
                }

                Core\write_to_log(print_r($gal_images, true));*/


    }
}

add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);
add_action('before_delete_post', '\\Omneo\Content\delete_content_item', 10, 3);