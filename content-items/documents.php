<?php namespace Omneo\Content\Item\Documents;

use Omneo\Core;
use Omneo\Content;

defined('ABSPATH') or die('Access Denied');


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 * @param $post
 * @param $updated
 */
function save_post($post_id, $post, $updated)
{
    $content_type_id = get_field('content_type', $post_id);

    if ($post->post_type == 'content_items' && $updated && $content_type_id == 4 ) {
        $file = get_field('file', $post_id);

        // Data
        $data = [
            // Optional
            'item_reference' => isset($file['url']) ? $file['url'] : ''
        ];

        Content\process_request([
            'post_id' => $post_id,
            'post' => $post,
            'data' => $data
        ]);
    }
}
add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);
add_action('before_delete_post', '\\Omneo\Content\delete_content_item', 10, 3);