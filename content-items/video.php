<?php namespace Omneo\Content\Item\Video;
defined('ABSPATH') or die('Access Denied');


use Omneo\Core;
use Omneo\Content;

defined('ABSPATH') or die('Access Denied');


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 * @param $post
 * @param $updated
 */
function save_post($post_id, $post, $updated)
{
    $content_type_id = get_field('content_type', $post_id);

    if ($post->post_type == 'content_items' && $updated && $content_type_id == 5 )
    {

        $data = [];
        $acf = get_fields($post_id);

        $data['video_url'] = $acf['video_link'];
        $data['video_height'] = $acf['video_height'];
        $data['video_width'] = $acf['video_width'];


        Content\process_request([
            'post_id' => $post_id,
            'post' => $post,
            'data' => $data
        ]);
    }
}
add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);
add_action('before_delete_post', '\\Omneo\Content\delete_content_item', 10, 3);