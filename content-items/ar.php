<?php
namespace Omneo\Content\Item\Ar;

use Omneo\Core;
use Omneo\Content;


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 * @param $post
 * @param $updated
 */
function save_post($post_id, $post, $updated)
{
    $content_type_id = get_field('content_type', $post_id);

    if ($post->post_type == 'content_items' && $updated && $content_type_id == 10 ) {

        // Data

        $acf = get_fields($post_id);
        $link = array();


        $data['xml_element_reference'] = $acf['ar_image_id'];

        $data['thumbnail_type'] = $acf['thumbnail_type'] == 1 ? 'image' : 'video';


        $data['thumbnail_reference'] = $acf['thumbnail_type'] == 1 ? $acf['thumbnail_image']['url'] : $acf['thumbnail_video']['url'];
        $data['item_reference'] = $acf['thumbnail_type'] == 1 ? $acf['thumbnail_image']['url'] : $acf['thumbnail_video']['url'];
        $data['thumbnail_video_height'] = $acf['ar_video_height'];
        $data['thumbnail_video_width'] = $acf['ar_video_width'];

        $link['title'] = 'AR Link For - ' . $post->post_title;
        $link['description'] = 'AR Link Description For - ' . $post->post_title;

        $link['internal_content_item_id'] = get_field('omneo_id', $acf['ar_content_item_id']->ID);

        $link['link'] = $acf['ar_external_link_url'];
        $link['link_type'] = $acf['ar_link_type'];
        $link['external_link_load'] = $acf['ar_external_link_load'];

        $data['link'] = json_encode($link);

        $response = Content\process_request([
            'post_id' => $post_id,
            'post' => $post,
            'data' => $data
        ]);

       // LINK

        if($response['data']['link_details']['id'])
        {


            $args['api_request'] = 'content/links/' .$response['data']['link_details']['id'];
            $args['data'] = $link;
            $args['verb'] = 'put';


            $link_response = Core\send_request($args);

        }
//        var_dump($response);
    }
}

add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);
add_action('before_delete_post', '\\Omneo\Content\delete_content_item', 10, 3);

/**
 * @param $field
 * @return mixed
 *
 * Populaters the AR Image Ref field from the XML file
 */
function omneo_trigger_load_options($field)
{
    // reset choices
    $field['choices'] = array();

    //grab the settings post
    $settings = get_posts(array(
        'post_type' => 'ar_settings',
    ));

    $xml_file = get_field('xml_file_reference', $settings[0]->ID, true);

    //no trigger map configured
    if (!$xml_file) {

        $field['choices'][0] = "Please upload triggers file";

        return $field;
    }

    //grab the nodes from the xml file
    $dom = simplexml_load_string(file_get_contents($xml_file['url']));

    foreach ($dom->{'Tracking'}->children() as $option) {

        $attributes = $option->attributes();

        $choices[(string)$attributes['name']] = (string)$attributes['name'];
    }

    // loop through array and add to field 'choices'
    if (is_array($choices)) {
        foreach ($choices as $choice) {
            $field['choices'][$choice] = $choice;
        }
    }

    // Important: return the field
    return $field;
}

// v4.0.0 and above
add_filter('acf/load_field/name=ar_image_id', __NAMESPACE__ . '\\omneo_trigger_load_options');

/**
 * @param $mimes
 * @return array
 *
 * Allow .dat & .xml uploads from the Media Manager
 */
function custom_upload_ar($mimes)
{
    $mimes = array_merge($mimes, array('xml' => 'application/xml'));
    $mimes = array_merge($mimes, array('dat' => 'application/data'));
    return $mimes;
}

add_filter('upload_mimes', __NAMESPACE__ . '\\custom_upload_ar');
