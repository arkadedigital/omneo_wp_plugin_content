<?php namespace Omneo\Content;

/**
 * Plugin Name:     Omneo Content
 * Plugin URI:      http://www.omneo.com.au
 * Version:         1.0.43
 * Description:     Omneo content items plugin
 */


use DateTime;
use Omneo\Core;
use Omneo\Content\Item;

defined('ABSPATH') or die('Access Denied');

global $CONTENT_TYPE_LIST;
$CONTENT_TYPE_LIST = [
    1  => 'html',
    2  => 'gallery',
    3  => 'links',
    4  => 'documents',
    5  => 'video',
    6  => 'events',
    7  => 'lookbooks',
    8  => 'accordion',
    9  => 'images',
    10 => 'augmentedreality',
    11 => 'redeemable'
];


require_once(WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'omneo-core/omneo-core.php');


function __PATH()
{
    return plugin_dir_path(__FILE__);
}

// Load acf json files
Core\register_acf_from_json(__PATH());

// Load content items post type
require_once(__PATH() . '/post-type.php');

require_once(__PATH() . 'lib/init.php');

// Includes
require_once(__PATH() . '/content-items/html.php');
require_once(__PATH() . '/content-items/accordion.php');
require_once(__PATH() . '/content-items/documents.php');
require_once(__PATH() . '/content-items/event.php');
require_once(__PATH() . '/content-items/gallery.php');
require_once(__PATH() . '/content-items/image.php');
require_once(__PATH() . '/content-items/links.php');
require_once(__PATH() . '/content-items/lookbook.php');
require_once(__PATH() . '/content-items/video.php');
require_once(__PATH() . '/content-items/ar.php');
require_once(__PATH() . '/content-items/redeemable.php');

require_once(__PATH() . 'ar-settings.php');
require_once(__PATH() . 'omneo-feedback.php');

require_once(__PATH() . 'widget/main.php');

function save_content_item($post_id, $post, $update)
{
    //global $CONTENT_TYPE_LIST;

    //$content_type = (($CONTENT_TYPE_LIST[$_POST['acf']['field_53227d84da8b0']]));

    //call_user_func('\Omneo\Content\Item\\' . ucwords($content_type) . '\\save_post'  ,$post_id, $post, $update );


}


/**
 * Delete content item
 *
 * @param $pid
 */
function delete_content_item($pid)
{
    $post_type = get_post_type($pid);
    global $CONTENT_TYPE_LIST;

    if ($post_type == 'content_items') {

        $omneo_id = get_field('omneo_id', $pid);
        $content_type = get_field('content_type', $pid);

        $args = [
            'api_request' => 'content/' . $CONTENT_TYPE_LIST[$content_type] . '/' . $omneo_id,
            'verb'        => 'delete'
        ];

        $response = Core\send_request($args);

        if (isset($response['error']) || !isset($response['data'])) {
            // Change post status to draft
            global $wpdb;
            $wpdb->update('wp_posts', ['post_status' => 'draft'], ['ID' => $args['post_id']]);

            // API Error
            if (isset($response['error']['message'])) {
                add_action('admin_notices', function ($response) {
                    //echo "<div class='error'> <p>" . $response['error']['message'] . "</p></div>";
                });
                // die($response['error']['message']);
            } else {
                // die('Omneo API Error');
            }

        }
    }
}

function set_common_post_acf_fields($data, $content_type_id, $post_id)
{
    try {

        update_field('field_5611e1e45b5c7', $data['id'], $post_id); // Omneo resource id
        update_field('field_56787395455ca', $data['content_item_attributes']['id'], $post_id); // Omneo content item id
        update_field('field_565d18a49d7b7', $data['content_item_attributes']['updated_at'], $post_id);
        update_field('field_56aaa4cf2565c', date('Ymd', time()), $post_id); // Publish date
        update_field('field_53227d84da8b0', $content_type_id, $post_id);

        update_field('field_53225d6cf6f36', $data['content_item_attributes']['description'], $post_id);
        update_field('field_53227f512969d', $data['content_item_attributes']['intro_content'], $post_id);

        update_field('field_53604103c1274', $data['content_item_attributes']['allow_comments'], $post_id);
        update_field('field_53604043c1272', $data['content_item_attributes']['allow_likes'], $post_id);
        update_field('field_56132ff343908', $data['content_item_attributes']['allow_ratings'], $post_id);
        update_field('field_574680ef48936', $data['content_item_attributes']['is_public'], $post_id);
        update_field('field_57467b5d48935', $data['content_item_attributes']['guest_only'], $post_id);

        // Google Analytics
        update_field('field_5614b6e8e9ab9', 1, $post_id);  // use_google_analytics default to on

        if (!empty($data['content_item_attributes']['thumbnail_off_image_url_attributes']['item_reference'])) {
            $img_id = upload_image_from_url($data['content_item_attributes']['thumbnail_off_image_url_attributes']['item_reference']);
            update_field('field_53227ba01b0e9', $img_id, $post_id);
        }

        if (!empty($data['content_item_attributes']['thumbnail_on_image_url_attributes']['item_reference'])) {
            $img_id = upload_image_from_url($data['content_item_attributes']['thumbnail_on_image_url_attributes']['item_reference']);
            update_field('field_5382da62ec6a2', $img_id, $post_id);

        }

        if (!empty($data['content_item_attributes']['thumbnail_lock_image_url_attributes']['item_reference'])) {
            $img_id = upload_image_from_url($data['content_item_attributes']['thumbnail_lock_image_url_attributes']['item_reference']);
            update_field('field_538d2e9b95166', $img_id, $post_id);

        }

        return true;
    } catch (Exception  $e) {
        Core\loggly('omneo-webbook', $e->getMessage());
        return $e->getMessage();
    }


}

/**
 * @param $url
 * @return null|string
 */
function upload_image_from_url($url)
{
    require_once(ABSPATH . 'wp-admin/includes/media.php');
    require_once(ABSPATH . 'wp-admin/includes/file.php');
    require_once(ABSPATH . 'wp-admin/includes/image.php');

    $image = \media_sideload_image($url, 0);

    $image_src = preg_replace('/.*(?<=src=["\'])([^"\']*)(?=["\']).*/', '$1', $image);

    $pos = strrpos($image_src, '/');
    $img_path = substr($image_src, $pos);


    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid like '%$img_path'";
    $id = $wpdb->get_var($query);

    return $id;
}


/**
 * Content item request wrapper
 *
 * @param $args
 * @return mixed
 */
function process_request($args)
{
    global $CONTENT_TYPE_LIST;

    $acf = get_fields($args['post_id']);

    $omneo_id = $acf['omneo_id'];
    $thumbnail_on_image = $acf['thumbnail_on'];
    $thumbnail_on_image_id = array_shift(get_post_meta($args['post_id'], 'thumbnail_on_image_id')); // image omneo resource id
    $thumbnail_off_image = $acf['thumbnail_off'];
    $thumbnail_off_image_id = array_shift(get_post_meta($args['post_id'], 'thumbnail_off_image_id')); // image omneo resource id
    $thumbnail_lock_image = $acf['thumbnail_lock'];
    $thumbnail_lock_image_id = array_shift(get_post_meta($args['post_id'], 'thumbnail_lock_image_id')); // image omneo resource id
    $published = $args['post']->post_status == 'publish' ? 1 : 0;
    $segments = isset($_POST['segments']) ? $_POST['segments'] : null;
    $segment_type = isset($_POST['segment_type']) ? $_POST['segment_type'] : 'is_public';
    $guest_only = $segment_type == 'guest_only' ? 1 : 0;
    $is_public = $segment_type == 'is_public' ? 1 : 0;


    update_field('field_574680ef48936', $is_public, $args['post_id']);
    update_field('field_57467b5d48935', $guest_only, $args['post_id']);


    // Unset segments if guest_only=0 or is_public=0
    if ($guest_only == 1 || $is_public == 1) {
        $segments = null;
    }

    // Common data fields
    $data = [
        // Required
        'title'                    => $args['post']->post_title,
        '_source'                  => 'wp',

        // Optional
        'description'              => $acf['description'],
        'intro_content'            => $acf['intro_content'],
        'thumbnail_on_image_url'   => isset($thumbnail_on_image['url']) ? $thumbnail_on_image['url'] : '',
        'thumbnail_off_image_url'  => isset($thumbnail_off_image['url']) ? $thumbnail_off_image['url'] : '',
        'thumbnail_lock_image_url' => isset($thumbnail_lock_image['url']) ? $thumbnail_lock_image['url'] : '',
        'promoted'                 => $acf['promoted'],
        'visibility'               => $acf['visibility'],
        'allow_comments'           => $acf['enable_comments'],
        'allow_likes'              => $acf['enable_likes'],
        'allow_ratings'            => $acf['enable_ratings'],
        'guest_only'               => $guest_only,
        'is_public'                => $is_public,
        'published'                => $published,
        'publish_date'             => $acf['publish_date'] ? strtotime('-10 hours', DateTime::createFromFormat('d/m/Y h:i a', $acf['publish_date'])->format('U')) : time(), // Default publish date to now if not set
        'unpublish_date'           => $acf['unpublish_date'] ? strtotime('-10 hours', DateTime::createFromFormat('d/m/Y h:i a', $acf['unpublish_date'])->format('U')) : 0,
        'item_order'               => $args['post']->menu_order,

        // Google Analytics
        'use_google_analytics'     => $acf['content_ga'],
        'utm_source'               => get_field('ga_utm_source', 'option'), // Global value
        'utm_medium'               => get_field('ga_utm_medium', 'option'), // Global value
        'utm_content'              => $acf['content_ga_utm_content'],
        'utm_term'                 => $acf['content_ga_utm_term'],
        'utm_campaign'             => $acf['content_ga_utm_campaign'] ?: get_field('ga_utm_campaign', 'option')
    ];

    // Set default acf publish_date if not set
    if (empty(get_field('publish_date', $args['post_id']))) {
        update_field('field_56aaa4cf2565c', date('Y-m-d H:i:s', strtotime('+10 hours', $data['publish_date'])), $args['post_id']);
    }


    // Save thumbnails (omneo image endpoint)
    $thumbnails = array('on', 'off', 'lock');
    foreach ($thumbnails as $thumbnail) {

        if (!${'thumbnail_' . $thumbnail . '_image'} && ${'thumbnail_' . $thumbnail . '_image_id'}) { // Delete

            $image_response = Core\send_request([
                'api_request' => 'content/images/' . ${'thumbnail_' . $thumbnail . '_image_id'},
                'verb'        => 'delete'
            ]);
            if (isset($image_response['data'])) {
                delete_post_meta($args['post_id'], 'thumbnail_' . $thumbnail . '_image_id', $image_response['data'][0]['id']);
                $data['thumbnail_' . $thumbnail . '_image_id'] = '';
            }
        } elseif (${'thumbnail_' . $thumbnail . '_image'} && empty(${'thumbnail_' . $thumbnail . '_image_id'})) { // Create


            $image_response = Core\send_request([
                'api_request' => 'content/images',
                'verb'        => 'post',
                'data'        => [
                    'title'          => 'Thumbnail - ' . $data['title'] . ' - ' . $thumbnail . ' Image',
                    'src_height'     => ${'thumbnail_' . $thumbnail . '_image'}['height'],
                    'src_width'      => ${'thumbnail_' . $thumbnail . '_image'}['width'],
                    'item_reference' => ${'thumbnail_' . $thumbnail . '_image'}['url']
                ]
            ]);


            if (isset($image_response['data'])) {


                if (${'thumbnail_' . $thumbnail . '_image_id'}) {
                    update_post_meta($args['post_id'], 'thumbnail_' . $thumbnail . '_image_id', $image_response['data'][0]['id']);
                } else {
                    add_post_meta($args['post_id'], 'thumbnail_' . $thumbnail . '_image_id', $image_response['data'][0]['id'], 1);
                }


                if ($image_response['data'][0]) {
                    $data['thumbnail_' . $thumbnail . '_image_id'] = $image_response['data'][0]['id'];
                } else {
                    $data['thumbnail_' . $thumbnail . '_image_id'] = array_shift($image_response['data']);
                }
            }
        } elseif (${'thumbnail_' . $thumbnail . '_image'} && ${'thumbnail_' . $thumbnail . '_image_id'}) { // Update


            $update_data = [
                'api_request' => 'content/images/' . ${'thumbnail_' . $thumbnail . '_image_id'},
                'verb'        => 'put',
                'data'        => [
                    'src_height'     => ${'thumbnail_' . $thumbnail . '_image'}['height'],
                    'src_width'      => ${'thumbnail_' . $thumbnail . '_image'}['width'],
                    'item_reference' => ${'thumbnail_' . $thumbnail . '_image'}['url']
                ]
            ];


            $image_response = Core\send_request($update_data);
            // var_dump($api_request)
            // var_dump($update_data);
            // var_dump($image_response);
            // die();


            if ($image_response['error']) {
                if ($image_response['error']['http_code'] != 404) {
                    if ($image_response['data'][0]) {
                        $data['thumbnail_' . $thumbnail . '_image_id'] = $image_response['data'][0]['id'];
                    } elseif (array_shift($image_response['data'])) {
                        $data['thumbnail_' . $thumbnail . '_image_id'] = array_shift($image_response['data']);
                    }
                } else {
                    //do something
                    //@TODO

                }
            }

            //update the thumbnail {whatever} image id
            if ($image_response['data'] && !$image_response['data'][0]) {
                $data['thumbnail_' . $thumbnail . '_image_id'] = ($image_response['data']['id']);
            }


        }

        //  echo $thumbnail;
        // var_dump(${'thumbnail_' . $thumbnail . '_id'});
        // var_dump(${'thumbnail_' . $thumbnail . '_image'});
    }

    //var_dump($data);
    //die() ;
    //


    $args['data'] = array_merge($args['data'], $data);

    $content_type_list_url = $CONTENT_TYPE_LIST[$acf['content_type']];
    $args['api_request'] = 'content/' . ($omneo_id ? $content_type_list_url . '/' . $omneo_id : $content_type_list_url);
    $args['verb'] = $omneo_id ? 'put' : 'post';


    // Make request

    //===


    /**
     *  Before a new content item is created the omneo ID is checked checked against the API to see if it exists.
     *  If no record is found on the API, omneo ID is removed from the wordpress record and saved as a new record on to the API
     */

    $check_args = $args;
    $check_args['verb'] = 'get';


    if ($check_args['verb'] == 'get') {
        if ($content_type_list_url == 'lookbooks') {
            unset($check_args['data']['lookbook_items']);
            //$check_args['data'] =    $check_args['data']['id'];
        }

    }

    $check_response = Core\send_request($check_args);


    if ($check_response['error'] && $omneo_id) {
        update_field('field_5611e1e45b5c7', '', $args['post_id']);
        $args['verb'] = 'post';
        $omneo_id = '';
        $args['api_request'] = 'content/' . ($omneo_id ? $content_type_list_url . '/' . $omneo_id : $content_type_list_url);
    }

    //var_dump($check_response);
    //var_dump($omneo_id);
    //die();


    //===

    $response = Core\send_request($args);

    // API Error
    if (isset($response['error']) || !isset($response['data'])) {
        // Change post status to pending
        global $wpdb;
        $wpdb->update('wp_posts', ['post_status' => 'pending'], ['ID' => $args['post_id']]);
        //update_field('field_5611e1e45b5c7', '', $args['post_id']);

        if (isset($response['error']['message'])) {
            add_action('admin_notices', function ($response) {
                echo "<div class='error'> <p>" . $response['error']['message'] . "</p></div>";
            });

            //die($response['error']['message']);
        } else {
            //die('Omneo API Error');
        }

    } elseif (!$omneo_id) { // Create
        // Update omneo id


        $response_item_id = $response['data']['id'];

        if ($content_type_list_url == 'images') {
            $response_item_id = $response['data'][0]['id'];

        }


        update_field('field_5611e1e45b5c7', $response_item_id, $args['post_id']); //omneo res ID
    }
    update_field('field_565d18a49d7b7', $response['data']['content_item_attributes']['updated_at'], $args['post_id']); //last updated


    update_field('field_56787395455ca', $response['data']['content_item_id'], $args['post_id']); //content_item_id


    $content_item_id = $response['data']['content_item_id'];


    // Save view items
//    if (is_plugin_active('omneo-views/omneo-views.php') && isset($_POST['views'])) {
    if (isset($_POST['views'])) {
        foreach ($_POST['views'] as $view_item) {


            if ($view_item['action'] == 'add') {
                $view_response = Core\send_request([
                    'api_request' => 'content/viewitems',
                    'verb'        => 'post',
                    'data'        => [
                        'view_id'         => $view_item['id'],
                        'content_item_id' => $content_item_id
                    ]
                ]);
            } elseif ($view_item['action'] == 'delete') {
                $view_response = Core\send_request([
                    'api_request' => 'content/viewitems/' . $view_item['id'],
                    'verb'        => 'delete'
                ]);
            }
        }
    }

    // Save locations
//    if (is_plugin_active('omneo-locations/omneo-locations.php') && isset($_POST['locations'])) {
    if (isset($_POST['locations'])) {
        foreach ($_POST['locations'] as $location) {
            if ($location['action'] == 'add') {
                $location_response = Core\send_request([
                    'api_request' => 'contentitemlocations',
                    'verb'        => 'post',
                    'data'        => [
                        'location_id'     => $location['id'],
                        'content_item_id' => $content_item_id
                    ]
                ]);
            } elseif ($location['action'] == 'delete') {
                $location_response = Core\send_request([
                    'api_request' => 'contentitemlocations/' . $location['id'],
                    'verb'        => 'delete'
                ]);
            }
        }
    }

    // Save content item segments
//    if (is_plugin_active('omneo-segments/omneo-segments.php') && isset($_POST['segments'])) {
    if (isset($_POST['segments'])) {
        foreach ($_POST['segments'] as $segment) {
            if ($segment['action'] == 'add') {
                $segment_response = Core\send_request([
                    'api_request' => 'contentsegments',
                    'verb'        => 'post',
                    'data'        => [
                        'segment_id'      => $segment['id'],
                        'content_item_id' => $content_item_id
                    ]
                ]);
            } elseif ($segment['action'] == 'delete') {
                $segment_response = Core\send_request([
                    'api_request' => 'contentsegments/' . $segment['id'],
                    'verb'        => 'delete'
                ]);
            }
        }
    }


    return $response;
}


function get_content_item_id($id, $type)
{
    $data['api_request'] = 'content/' . $type . '/' . $id;
    $data['data'] = array();
    $data['verb'] = 'GET';

    $response = Core\send_request($data);
    return $response['data']['content_item_id'];
}


function get_content_rating_types()
{
    $args['verb'] = 'GET';
    $args['api_request'] = "contentitemratingtypes";
    $args['data'] = array();

    $response = Core\send_request($args);

    //Core\write_to_log($response);

    return $response['data'];
}


function my_admin_error_notice($message, $class)
{
    echo "<div class=\"$class\"> <p>$message</p></div>";
}

/**
 * Load assets
 */
function load_assets()
{
    if ((isset($_GET['post']) && get_post_type($_GET['post']) == 'content_items') || (isset($_GET['post_type']) && $_GET['post_type'] == 'content_items')) {
        wp_enqueue_style('content', plugins_url() . DS . 'omneo-content' . DS . 'assets' . DS . 'main.css');
        wp_enqueue_script('content', plugins_url() . DS . 'omneo-content' . DS . 'assets' . DS . 'main.js');
    }
}

load_assets();

function testHMAC()
{
    $data['api_request'] = 'testHMAC';
    $data['data'] = array();
    $data['verb'] = 'get';

    $response = Core\send_request($data);
    Core\write_to_log($response);

}

//add_action('init', __NAMESPACE__ . '\\testHMAC');