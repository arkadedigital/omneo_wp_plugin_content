<?php

namespace Omneo\Content\Item\AugmentedRealityReference;

use Omneo\Core;
use Omneo\Content;

function save_ar_settings($post_id, $post, $updated)
{
    if ($post->post_type == 'ar_settings' && $updated)
    {
        $acf = get_fields($post_id);


        $data['title'] = $post->post_title;
        $data['item_reference'] = $acf['xml_file_reference']['url'];
        $data['item_datafile_reference'] = $acf['dat_file_reference']['url'];


        $args['api_request'] =    'content/augmentedrealityxmlreference';
        $args['post_id'] = $post_id;
        $args['data'] = $data;
        $args['verb'] = 'post';


        $response = Core\send_request($args);

        if (isset($response['error']))
        {


        } elseif(!$acf['omneo_id'])
        {
            // Update omneo id
            update_field('field_561b42e08dba0', $response['data']['id'], $args['post_id']);

        }

    }
}
add_action('save_post', __NAMESPACE__ . '\\save_ar_settings', 10, 3);
