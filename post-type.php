<?php namespace Omeno\Content\PostType;
defined('ABSPATH') or die('Access Denied');

function omneo_content_item_post_type()
{
    register_post_type('content_items', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
        array('labels' => array(
            'name' => __('Content Items'), /* This is the Title of the Group */
            'singular_name' => __('Content Item'), /* This is the individual type */
            'all_items' => __('All Content Items'), /* the all items menu item */
            'add_new' => __('Add New'), /* The add new menu item */
            'add_new_item' => __('Add New Content Item'), /* Add New Display Title */
            'edit' => __('Edit'), /* Edit Dialog */
            'edit_item' => __('Edit Content Items'), /* Edit Display Title */
            'new_item' => __('New Content Item'), /* New Display Title */
            'view_item' => __('View Content Item'), /* View Display Title */
            'search_items' => __('Search Content Item'), /* Search Custom Type Title */
            'not_found' => __('Nothing found in the Database.'), /* This displays if there are no entries yet */
            'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
        ), /* end of arrays */
            'description' => __('This is the "Content Items" section containing data to display on the App'), /* Custom Type Description */
            'public' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => null, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon' => 'dashicons-smartphone', /* the icon for the view type menu */
            'rewrite' => array('slug' => 'content_items', 'with_front' => false), /* you can specify its url slug */
            'has_archive' => 'content_items', /* you can rename the slug here */
            'capability_type' => 'post',
            'hierarchical' => false,
            /* the next one is important, it tells what's enabled in the post editor */
            'supports' => array('title', 'page-attributes', 'slug')
        ) /* end of options */

    ); /* end of register post type */
}

// adding the function to the Wordpress init
add_action('init', __NAMESPACE__ . '\\omneo_content_item_post_type');


/**
 *
 * AR SETTINGS
 *
 */
function omneo_ar_settings_post_type() {
    register_post_type('ar_settings', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
        array('labels' => array(
            'name' => __('AR Settings'), /* This is the Title of the Group */
            'singular_name' => __('AR Settings'), /* This is the individual type */
            'all_items' => __('AR Settings'), /* the all items menu item */
            'add_new' => __('AR Settings'), /* The add new menu item */
            'add_new_item' => __('AR Settings'), /* Add New Display Title */
            'edit' => __('AR Settings'), /* Edit Dialog */
            'edit_item' => __('AR Settings'), /* Edit Display Title */
            'new_item' => __('AR Settings'), /* New Display Title */
            'view_item' => __('AR Settings'), /* View Display Title */
            'search_items' => __('Search AR Settings'), /* Search Custom Type Title */
            'not_found' => __('Nothing found in the Database.'), /* This displays if there are no entries yet */
            'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
        ), /* end of arrays */
            'description' => __('This is the main section containing the settings for AR Feature'), /* Custom Type Description */
            'public' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => false,
            'show_ui' => true,
            'show_in_menu' => 'edit.php?post_type=content_items',
            'query_var' => true,
            'menu_position' => 99, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon' => 'dashicons-smartphone', /* the icon for the view type menu */
            'rewrite' => array('slug' => 'terms_conditions', 'with_front' => false), /* you can specify its url slug */
            'has_archive' => 'ar_settings', /* you can rename the slug here */
            'capability_type' => 'post',
            'hierarchical' => false,
            /* the next one is important, it tells what's enabled in the post editor */
            'supports' => array('title')
        ) /* end of options */
    ); /* end of register post type */

}

// Hook into the 'init' action
add_action( 'init',  __NAMESPACE__ . '\\omneo_ar_settings_post_type', 0 );


add_action('init',  __NAMESPACE__ . '\\omneo_activate_ar_settings');

function omneo_activate_ar_settings() {
    $settings = new \WP_Query();
    $settings->query(array(
        'posts_per_page' => -1,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post_type' => 'ar_settings'
    ));

    if(!$settings->have_posts()){

        $add_default_page = array(
            'post_title' => 'AR Settings',
            'post_content' => '',
            'post_name' => 'ar_settings',
            'menu_order' => '1',
            'post_status' => 'publish',
            'post_type' => 'ar_settings'
        );

        // insert the post into the database
        wp_insert_post($add_default_page);
    }
}