<?php

use Omeno\Core;
use Omeno\Content;
use Omeno\Content\Feedback;

function boxed_add_metaboxes($page, $post)
{
        add_meta_box('omneo-content-item-side-meta-box','Feedback','omneo_content_item_side_meta_box',$page,'side','low');
}
add_action('add_meta_boxes','boxed_add_metaboxes',10,2);


function omneo_content_item_side_meta_box()
{
    global $post;
    global $CONTENT_TYPE_LIST;

    $acf = get_fields($post->ID);
    $likes = Feedback\get_content_feedback($post, 'contentitemlikes');
    $comments = Feedback\get_content_feedback($post, 'contentitemcomments');


    $ratings = Feedback\get_content_feedback($post, 'contentitemratings');

    //build ratings html
    $ratings_html = '';
    if(!empty($ratings))
    {
        foreach($ratings as  $rating_id => $r)
        {
            $ratings_html .= $r['title'] . " (" . count($r['responses']) . ") <br />";
        }
    }

    $content_type = $CONTENT_TYPE_LIST[$acf['content_type']];


    if($likes > 0)
    {
        echo 'Likes : ' . $likes . "<br>";
    }else
    {
        echo 'No Likes <br >';
    }

    if($comments > 0)
    {
        echo " <a href='/wp-admin/post.php?page=list-comments&content_item_id=" .
            get_field('omneo_content_item_id', $_GET['post'])
            ."&post_id={$_GET['post']}'>View Comments</a> <br>";
    }else
    {
        echo 'No Comments<br >';
    }


    if($ratings_html != '')
    {

        echo " <a href='/wp-admin/post.php?page=list-ratings&content_item_id=" .
            get_field('omneo_content_item_id', $_GET['post'])
            ."&post_id={$_GET['post']}'>View  Ratings</a> <br>";

        echo '' . $ratings_html;
    }else
    {
        echo 'No Ratings<br >';
    }
}



