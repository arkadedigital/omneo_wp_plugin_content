<?php

use Omeno\Core;
use Omeno\Content;
use Omeno\Content\Feedback;

global $post;



if($_GET['action'] == 'delete' && !empty($_GET['id']))
{

    $args['verb'] = 'delete';
    $args['api_request'] = "contentitemratings/{$_GET['id']}";
    $args['data'] = array();
    $response = \Omneo\Core\send_request($args);

    //wp_redirect('/wp-admin/post.php?page=list-comments&content_item_id=' . $content_item_id);


}


$post = get_post($_GET['post_id']);

$content_item_id = sanitize_title($_GET['content_item_id']);

$ratings = Feedback\get_content_feedback($post, 'contentitemratings');



?>
<div id="poststuff">

    <h2>Ratings for Content Item - <?php echo $post->post_title ?></h2>
    <a class="button button-primary" href="/wp-admin/post.php?post=<?php echo $post->ID ?>&action=edit">< Back to editing post</a>

    <hr>
                <?php foreach($ratings as $rating_type_id => $rt) :?>
                    <?php $values = (json_decode($rt['options']));
                    ?>
                    <h2 style="padding: 10px"><?php echo $rt['title']?> (<?php echo count($rt['responses'])?>)</h2>
    <div id="acf-" class="postbox  acf-postbox default ">
        <div class="inside acf-fields acf-cf">
            <div class="inside acf-fields acf-cf">
                <h4 style="padding: 5px">Available Options Values <?php echo $rt['options']?></h4>
                <table class="table table-striped" cellpadding="5" width="100%">
                    <thead>
                    <tr>
                        <td width="10%">#</td>
                        <td width="30%">Rating Value</td>
                        <td width="40%">Date</td>
                        <td width="10%">&nbsp;</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($rt['responses'] as $r):?>
                        <tr>
                            <td><?php echo $r['id'] ?></td>
                            <td><?php echo $r['value'] ?></td>
                            <td><?php echo date('d M Y H:ia', $r['created_at']) ?></td>
                            <td><a href="/wp-admin/post.php?page=list-ratings&content_item_id=<?php echo $content_item_id ?>&post_id=<?php echo $post->ID ?>&action=delete&id=<?php echo $r['id'] ?>"> Delete </a></td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>

            </div>


        </div>
    </div>

                <?php endforeach; ?>
</div>

<?php //var_dump($ratings)?>