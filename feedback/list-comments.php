<?php

use Omeno\Core;
use Omeno\Content;


if($_GET['action'] == 'delete' && !empty($_GET['id']))
{

    $args['verb'] = 'delete';
    $args['api_request'] = "contentitemcomments/{$_GET['id']}";
    $args['data'] = array();
    $response = \Omneo\Core\send_request($args);

    //wp_redirect('/wp-admin/post.php?page=list-comments&content_item_id=' . $content_item_id);


}



$content_item_id = sanitize_title($_GET['content_item_id']);

$args['verb'] = 'get';
$args['api_request'] = "contentitemcomments?content_item_id={$content_item_id}";
$args['data'] = array();

$response = \Omneo\Core\send_request($args);

//var_dump($response);
$post =   get_post($_GET['post_id']);



?>
<div id="poststuff">

    <h2>Comments for Content Item - <?php echo $post->post_title ?></h2>
    <a class="button button-primary" href="/wp-admin/post.php?post=<?php echo $post->ID ?>&action=edit">< Back to editing post</a>

    <hr>
    <div id="acf-" class="postbox  acf-postbox default ">
        <div class="inside acf-fields acf-cf">
            <div class="inside acf-fields acf-cf">


                <table class="table table-striped" cellpadding="5" width="100%">
                    <thead>
                    <tr>
                        <td width="20%">User</td>
                        <td width="30%">Comment</td>
                        <td width="40%">Date</td>
                        <td width="10%">&nbsp;</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($response['data'] as $r):?>
                        <tr>
                            <td><?php echo $r['user_id'] ?></td>
                            <td><?php echo $r['comment'] ?></td>
                            <td><?php echo date('d M Y H:ia', $r['created_at']) ?></td>
                            <td><a href="/wp-admin/post.php?page=list-comments&content_item_id=<?php echo $content_item_id ?>&action=delete&id=<?php echo $r['id'] ?>"> Delete </a></td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>


        </div>
    </div>

</div>
